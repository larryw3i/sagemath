==================
SageMath in Debian
==================

Build instructions
==================

See https://wiki.debian.org/DebianScience/Sage for instructions on setting your
system up to work with this source package.

Local system patches
--------------------

No local system changes should be needed at this time for the main and dochtml
builds to work automatically from a clean check-out.

Remaining tasks
===============

See TODO and https://wiki.debian.org/DebianScience/Sage

Running doctests
================

(These instructions are for running doctests from a built source tree. For
running doctests from an installed binary package, see README.Debian.)

The tests write logging output to sage/logs/ptestlong.log, or some other file
if you ran a different test. If you let the test run to completion, near the
end of the logging output, there will be a summary of all the failures.

After a successful build, you can run individual tests as follows. First, make
sure you're in the top-level package directory, where debian/ is. Then:

 $ cd sage && ./sage -t -p --long src/sage/file/you/want/to/test

To run all the tests:

 sage$ cd sage && ./sage -t -p --all --long --logfile=logs/ptestlong.log

See https://www.sagemath.org/git-developer-guide/doctesting.html for more
information and helpful tips.

Fixing test failures
--------------------

There are three cases:

- Fixing a doctest. That is, something that looks like this:

  [.. sage python/cython code ..]
  """Some documentation

  sage: (code to be tested)
  (expected output)
  """
  [.. sage python/cython code ..]

  This is the easiest case - simply edit the file in-place in sage/src, then
  re-run the tests as described above.

- Fixing python code. That is, code in a .py file that is not a doctest.

  Edit the file in-place in sage/src/a/b/etc, then copy it over to
  local/lib/python3.*/site-packages/a/b/etc (or debian/tmp if
  `dh_auto_install` had previously succeeded) then run the tests again.

- Fixing cython code. That is, code in a .pyx file that is not a doctest.

  You'll need to re-run the build again.

You may also find this useful:

 $ debian/rules check-failed

It will run all previously-failed tests again, which is useful if you're on a
mass test-fixing spree and forget to count which ones you've fixed. However
this assumes that you didn't break any of the previously successful ones :p

Finally, after you are satisfied with your fixes, you should do a completely
clean build to verify these fixes - to make sure that they weren't affected by
temporary intermediate build products, e.g. due to our short-cuts above.

Current test status
-------------------

You should get something roughly like the following. If you get different
results, please add your experience to the below summary.

An overview over test failures on different architectures on buildds can be found at
https://people.debian.org/~thansen/sage-test-status.html

You can see infinity0's test failures here: https://people.debian.org/~infinity0/sage/
Look for files named "sagemath-*_*.log"

Known causes::

sage -t --long src/sage/misc/sagedoc.py  # 7 doctests failed // obviously fails when the documentation is not built

Manual testing
==============

Examples
--------

These can be tested from the Sage CLI and the Jupyter Notebook.

Typesetting::

    show(integrate(1/(1+x^4), x))

    You should get a long expression with arctans, logs, and square roots.

3D plots::

    def f(x,y):
        return math.sin(y*y+x*x)/math.sqrt(x*x+y*y+.0001)
    P = plot3d(f,(-3,3),(-3,3), adaptive=True, color=rainbow(60, 'rgbtuple'), max_bend=.1, max_depth=15)
    P.show()

    You should get a 3D plot, or a notice about JSmol being unavailable.
