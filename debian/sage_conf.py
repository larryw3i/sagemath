r"""
Sage variable definitions for Debian
"""
SAGE_DOC                      = '/usr/share/doc/sagemath'

CONWAY_POLYNOMIALS_DATA_DIR   = '/usr/share/sagemath/conway_polynomials'
GRAPHS_DATA_DIR               = '/usr/share/sagemath/graphs'
ELLCURVE_DATA_DIR             = '/usr/share/sagemath/ellcurves'
POLYTOPE_DATA_DIR             = '/usr/share/sagemath/reflexive_polytopes'
GAP_ROOT_DIR                  = '/usr/share/gap'
THEBE_DIR                     = '/usr/share/thebe'
COMBINATORIAL_DESIGN_DATA_DIR = '/usr/share/sagemath/combinatorial_designs'
CREMONA_MINI_DATA_DIR         = '/usr/share/sagemath/cremona'

JMOL_DIR                      = '/usr/share/jmol'
MATHJAX_DIR                   = '/usr/share/javascript/mathjax'
THREEJS_DIR                   = '/usr/share/javascript/three'

PPLPY_DOCS                    = '/usr/share/doc/python3-ppl/html'
MAXIMA                        = 'maxima-sage'
MAXIMA_FAS                    = '/usr/lib/ecl/maxima.fas'
SAGE_NAUTY_BINS_PREFIX        = 'nauty-'
ARB_LIBRARY                   = 'flint-arb'
LIBSINGULAR_PATH              = 'libsingular-Singular.so'
